import React from 'react';
import logo from './logo.svg';
import './App.css';
import principal from './components/principal'

ReactDOM.render(<principal />, document.querySelector('.Principal'));
function App() {
  return (
    <div className="App">
      <header className="App-header"></header>
      <main className="Principal" />
    </div>
  );
}

export default App;
